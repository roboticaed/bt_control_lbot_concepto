#include <SoftwareSerial.h>
#include <math.h>

SoftwareSerial SSerial(11, 12);

unsigned m0_pwm = 13;
unsigned m0_dir = 4;
unsigned m1_pwm = 6;
unsigned m1_dir = 8;

void setup() {
  // put your setup code here, to run once:
  SSerial.begin(9600);
  pinMode(m0_pwm, OUTPUT);
  pinMode(m0_dir, OUTPUT);
  pinMode(m1_pwm, OUTPUT);
  pinMode(m1_dir, OUTPUT);
}

void setMotor(int port1, int port2, int vel)
{
  long velo=vel;
  int digState=LOW;
  long finalVelo=0;
  if(velo==0)
  {
    digState=LOW;
    finalVelo=0;
  }

  if(velo>0)
  {
    digState=LOW;
    finalVelo=round((155.0/99.0)*velo+(9745.0/99.0));
    if(finalVelo>255) finalVelo=255;
  }

  if(velo<0)
  {
    digState=HIGH;
    finalVelo=round((155.0/99.0)*velo+(15500.0/99.0));
    if(finalVelo<0) finalVelo=0;
  }
  analogWrite(port1,finalVelo);
  digitalWrite(port2,digState);
}

void forward(int vel) {
  setMotor(m0_pwm, m0_dir, vel);
  setMotor(m1_pwm, m1_dir, vel);
}

void backward(int vel) {
  setMotor(m0_pwm, m0_dir, -vel);
  setMotor(m1_pwm, m1_dir, -vel);
}

void left(int vel) {
  setMotor(m0_pwm, m0_dir, vel);
  setMotor(m1_pwm, m1_dir, -vel);
}

void right(int vel) {
  setMotor(m0_pwm, m0_dir, -vel);
  setMotor(m1_pwm, m1_dir, vel);
}


void loop() {
  // put your main code here, to run repeatedly:
  if (SSerial.available() > 0){
    unsigned char c;
    c = SSerial.read();
    switch(c){
      case 'w':
        forward(100);
        break;
      case 's':
        backward(100);
        break;
      case 'a':
        left(100);
        break;
      case 'd':
        right(100);
        break;
      case ' ':
        forward(0);
        break;
      case 'v':
        SSerial.println(analogRead(A0));
        break;
    }
  }
}
