import serial
import struct

class LBot(object):
    def __init__(self, device='/dev/rfcomm0', baudrate=9600, timeout=0.2):
        self.sp = serial.Serial(
            device,
            baudrate=baudrate,
            timeout=timeout
        )

    def analog(self):
        self.sp.write(b'v ')
        answer = self.sp.read(2)
        if len(answer) < 2:
            return -1
        return struct.unpack('<H', answer)[0]

    def _vel_param(self, vel):
        vel = vel // 10
        if vel >= 10:
            vel = 9
        return '{}'.format(vel).encode('ascii')

    def forward(self, vel):
        vel = self._vel_param(vel)
        self.sp.write(b'f' + vel)
        print('forward {}\n'.format((b'f' + vel).decode('ascii')))

    def backward(self, vel):
        vel = self._vel_param(vel)
        self.sp.write(b'b' + vel)

    def left(self, vel):
        vel = self._vel_param(vel)
        self.sp.write(b'l' + vel)

    def right(self, vel):
        vel = self._vel_param(vel)
        self.sp.write(b'r' + vel)

if __name__ == '__main__':
    import time
    sp = serial.Serial(sys.argv[1], baudrate=9600)
    lbot = LBotito(sp)
    input('BT asociado?')
    while True:
        for i in range(0, 100, 10):
            lbot.forward(i)
            time.sleep(0.5)
