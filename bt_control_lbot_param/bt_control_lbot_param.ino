#include <SoftwareSerial.h>
#include <math.h>

SoftwareSerial SSerial(11, 12);

unsigned m0_pwm = 13;
unsigned m0_dir = 4;
unsigned m1_pwm = 6;
unsigned m1_dir = 8;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(57600);
  SSerial.begin(9600);
  pinMode(m0_pwm, OUTPUT);
  pinMode(m0_dir, OUTPUT);
  pinMode(m1_pwm, OUTPUT);
  pinMode(m1_dir, OUTPUT);
}

void setMotor(int port1, int port2, int vel)
{
  long velo=vel;
  int digState=LOW;
  long finalVelo=0;
  if(velo==0)
  {
    digState=LOW;
    finalVelo=0;
  }

  if(velo>0)
  {
    digState=LOW;
    finalVelo=round((155.0/99.0)*velo+(9745.0/99.0));
    if(finalVelo>255) finalVelo=255;
  }

  if(velo<0)
  {
    digState=HIGH;
    finalVelo=round((155.0/99.0)*velo+(15500.0/99.0));
    if(finalVelo<0) finalVelo=0;
  }
  analogWrite(port1,finalVelo);
  digitalWrite(port2,digState);
}

void forward(int vel) {
  setMotor(m0_pwm, m0_dir, vel);
  setMotor(m1_pwm, m1_dir, vel);
}

void backward(int vel) {
  setMotor(m0_pwm, m0_dir, -vel);
  setMotor(m1_pwm, m1_dir, -vel);
}

void left(int vel) {
  setMotor(m0_pwm, m0_dir, vel);
  setMotor(m1_pwm, m1_dir, -vel);
}

void right(int vel) {
  setMotor(m0_pwm, m0_dir, -vel);
  setMotor(m1_pwm, m1_dir, vel);
}


void loop() {
  static unsigned bytes = 0;
  static unsigned char command[2];
  unsigned analog_value;
  unsigned char answer[2];
  // put your main code here, to run repeatedly:
  if (bytes == 2){
    Serial.print("Command: ");
    Serial.print((char) command[0]);
    Serial.println((char) command[1]);
    int arg = command[1] - '0';
    arg = (arg >= 0 && arg <= 9)?arg:0;
    bytes = 0;
    switch(command[0]){
      case 'f':
        forward(arg * 10);
        break;
      case 'b':
        backward(arg * 10);
        break;
      case 'l':
        left(arg * 10);
        break;
      case 'r':
        right(arg * 10);
        break;
      case ' ':
        forward(0);
        break;
      case 'v':
        analog_value = analogRead(A0);
        answer[0] = analog_value % 256;
        answer[1] = analog_value / 256;
        SSerial.write(answer[0]);
        SSerial.write(answer[1]);
        break;
    }  
  }
  if (SSerial.available() > 0 && bytes < 2){
    command[bytes++] = SSerial.read();
  }
}
